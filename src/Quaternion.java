import java.util.Objects;
import java.util.StringTokenizer;

/** Quaternions. Basic operations. */
public class Quaternion {

    private double a;
    private double b;
    private double c;
    private double d;

    /** Constructor from four double values.
     * @param a real part
     * @param b imaginary part i
     * @param c imaginary part j
     * @param d imaginary part k
     */
    public Quaternion(double a, double b, double c, double d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    /** Real part of the quaternion.
     * @return real part
     */
    public double getRpart() {
        return a;
    }

    /** Imaginary part i of the quaternion.
     * @return imaginary part i
     */
    public double getIpart() {
        return b;
    }

    /** Imaginary part j of the quaternion.
     * @return imaginary part j
     */
    public double getJpart() {
        return c;
    }

    /** Imaginary part k of the quaternion.
     * @return imaginary part k
     */
    public double getKpart() {
        return d;
    }

    /** Conversion of the quaternion to the string.
     * @return a string form of this quaternion:
     * "a+bi+cj+dk"
     * (without any brackets)
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(a);
        if (b >= 0) {
            stringBuilder.append("+");
        }
        stringBuilder.append(b);
        stringBuilder.append("i");
        if (c >= 0) {
            stringBuilder.append("+");
        }
        stringBuilder.append(c);
        stringBuilder.append("j");
        if (d >= 0) {
            stringBuilder.append("+");
        }
        stringBuilder.append(d);
        stringBuilder.append("k");
        return stringBuilder.toString();
    }

    /** Conversion from the string to the quaternion.
     * Reverse to <code>toString</code> method.
     * @throws IllegalArgumentException if string s does not represent
     *     a quaternion (defined by the <code>toString</code> method)
     * @param s string of form produced by the <code>toString</code> method
     * @return a quaternion represented by string s
     */
    public static Quaternion valueOf(String s) {
        if (s == null || s.isEmpty()) {
            throw new IllegalArgumentException("String " + s + " does not represent quaternion");
        }
        if(!checkAmountOfDelimiters(s)){
            throw new IllegalArgumentException("String " + s + " does not represent quaternion");
        }
        s = s.trim();
        StringTokenizer stringTokenizer = new StringTokenizer(s, "+-");
        double a = 0;
        double b = 0;
        double c = 0;
        double d = 0;
        for (int i = 0; i < 4; i++) {
            if (!stringTokenizer.hasMoreTokens()) {
                throw new IllegalArgumentException("String " + s + " does not represent quaternion");
            }
            if (i == 0) {
                String token = stringTokenizer.nextToken();
                int indexOfToken = s.indexOf(token);
                if (isNumeric(token)) {
                    a = Double.parseDouble(token);
                } else {
                    throw new IllegalArgumentException("String " + s + " does not represent quaternion");
                }
                if (indexOfToken != 0) {
                    a = -a;
                }
            }
            if (i == 1) {
                String token = stringTokenizer.nextToken();
                int indexOfToken = s.indexOf(token);
                try {
                    b = addMinusIfNeeded(s, indexOfToken, getImaginaryPartFromString(token, 'i'));
                }catch (Exception e){
                    throw new IllegalArgumentException("String " + s + " does not represent quternion");
                }
            }
            if (i == 2) {
                String token = stringTokenizer.nextToken();
                int indexOfToken = s.indexOf(token);
                try {
                    c = addMinusIfNeeded(s, indexOfToken, getImaginaryPartFromString(token, 'j'));
                }catch (Exception e){
                    throw new IllegalArgumentException("String " + s + " does not represent quternion");
                }
            }
            if (i == 3) {
                String token = stringTokenizer.nextToken();
                int indexOfToken = s.indexOf(token);
                try{
                d = addMinusIfNeeded(s, indexOfToken, getImaginaryPartFromString(token, 'k'));
                }catch (Exception e){
                    throw new IllegalArgumentException("String " + s + " does not represent quternion");
                }

            }

        }
        if (stringTokenizer.hasMoreTokens()) {
            throw new IllegalArgumentException("String " + s + " does not represent quaternion");
        }
        return new Quaternion(a, b, c, d);
    }

    private static boolean checkAmountOfDelimiters(String s) {
        int count = 0;
        for(char c : s.toCharArray()){
            if(c == '-' || c == '+'){
                count++;
            }
        }
        if(s.charAt(0) != '-'){
            return count == 3;
        }
        return count == 4;
    }

    private static double addMinusIfNeeded(String string, int indexOfToken, double a) {
        if (string.charAt(indexOfToken - 1) == '-') {
            return -a;
        }
        return a;
    }

    private static double getImaginaryPartFromString(String string, char imPartChar) throws Exception {
        if (string.charAt(string.length() - 1) == imPartChar
                && isNumeric(string.substring(0, string.length() - 1))) {
            return Double.parseDouble(string.substring(0, string.length() - 1));
        } else {
            throw new Exception("String " + string + " does not represent quaternion");
        }
    }

    private static boolean isNumeric(String string) {
        try {
            Double.parseDouble(string);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /** Clone of the quaternion.
     * @return independent clone of <code>this</code>
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Quaternion(a, b, c, d);
    }

    /** Test whether the quaternion is zero.
     * @return true, if the real part and all the imaginary parts are (close to) zero
     */
    public boolean isZero() {
        if (isEqualUsingThreshold(a, 0)
                && isEqualUsingThreshold(b, 0)
                && isEqualUsingThreshold(c, 0)
                && isEqualUsingThreshold(d, 0)) {
            return true;
        }
        return false;
    }

    /** Conjugate of the quaternion. Expressed by the formula
     *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
     * @return conjugate of <code>this</code>
     */
    public Quaternion conjugate() {
        return new Quaternion(a, -b, -c, -d);
    }

    /** Opposite of the quaternion. Expressed by the formula
     *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
     * @return quaternion <code>-this</code>
     */
    public Quaternion opposite() {
        return new Quaternion(-a, -b, -c, -d);
    }

    /** Sum of quaternions. Expressed by the formula
     *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
     * @param q addend
     * @return quaternion <code>this+q</code>
     */
    public Quaternion plus(Quaternion q) {
        return new Quaternion(a + q.getRpart(), b + q.getIpart(), c + q.getJpart(), d + q.getKpart());
    }

    /** Product of quaternions. Expressed by the formula
     *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
     *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
     *  b = i
     *  c = j
     *  d = k
     * @param q factor
     * @return quaternion <code>this*q</code>
     */
    public Quaternion times(Quaternion q) {
        double a = this.a * q.getRpart() - this.b * q.getIpart() - this.c * q.getJpart() - this.d * q.getKpart();
        double b = this.a * q.getIpart() + this.b * q.getRpart() + this.c * q.getKpart() - this.d * q.getJpart();
        double c = this.a * q.getJpart() - this.b * q.getKpart() + this.c * q.getRpart() + this.d * q.getIpart();
        double d = this.a * q.getKpart() + this.b * q.getJpart() - this.c * q.getIpart() + this.d * q.getRpart();
        return new Quaternion(a, b, c, d);
    }

    /** Multiplication by a coefficient.
     * @param r coefficient
     * @return quaternion <code>this*r</code>
     */
    public Quaternion times(double r) {
        double a = this.a * r;
        double b = this.b * r;
        double c = this.c * r;
        double d = this.d * r;
        return new Quaternion(a, b, c, d);
    }

    /** Inverse of the quaternion. Expressed by the formula
     *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
     *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
     * @return quaternion <code>1/this</code>
     */
    public Quaternion inverse() {
        if (this.isZero()) {
            throw new RuntimeException("Cannot inverse zero Quaternion");
        }
        double a = inverseMultiplier(this.a);
        double b = inverseMultiplier(-this.b);
        double c = inverseMultiplier(-this.c);
        double d = inverseMultiplier(-this.d);
        return new Quaternion(a, b, c, d);
    }

    private double inverseMultiplier(double element) {
        return element / (a * a + b * b + c * c + d * d);
    }

    /** Difference of quaternions. Expressed as addition to the opposite.
     * @param q subtrahend
     * @return quaternion <code>this-q</code>
     */
    public Quaternion minus(Quaternion q) {
        return this.plus(q.opposite());
    }

    /** Right quotient of quaternions. Expressed as multiplication to the inverse.
     * @param q (right) divisor
     * @return quaternion <code>this*inverse(q)</code>
     */
    public Quaternion divideByRight(Quaternion q) {
        if(!q.isZero()) {
            return this.times(q.inverse());
        }else{
            throw new RuntimeException("Cannot divide by Zero quaternion");
        }
    }

    /** Left quotient of quaternions.
     * @param q (left) divisor
     * @return quaternion <code>inverse(q)*this</code>
     */
    public Quaternion divideByLeft(Quaternion q) {
        if(!q.isZero()) {
            return q.inverse().times(this);
        }else{
            throw new RuntimeException("Cannot divide by Zero Quaternion");
        }
    }

    /** Equality test of quaternions. Difference of equal numbers
     *     is (close to) zero.
     * @param qo second quaternion
     * @return logical value of the expression <code>this.equals(qo)</code>
     */
    @Override
    public boolean equals(Object qo) {
        if (qo instanceof Quaternion) {

            if (isEqualUsingThreshold(a, ((Quaternion) qo).getRpart())
                    && isEqualUsingThreshold(b, ((Quaternion) qo).getIpart())
                    && isEqualUsingThreshold(c, ((Quaternion) qo).getJpart())
                    && isEqualUsingThreshold(d, ((Quaternion) qo).getKpart())) {
                return true;
            }
        }
        return false;
    }

    private boolean isEqualUsingThreshold(double a, double b) {
        final double THRESHOLD = 0.001;
        if (Math.abs(a - b) < THRESHOLD) {
            return true;
        }
        return false;
    }

    /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
     * @param q factor
     * @return dot product of this and q
     */
    public Quaternion dotMult(Quaternion q) {
        Quaternion result = this.times(q.conjugate()).plus(q.times(this.conjugate()));
        return new Quaternion(result.getRpart() / 2, result.getIpart() / 2, result.getJpart() / 2, result.getKpart() / 2);
    }

    /** Integer hashCode has to be the same for equal objects.
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(a, b, c, d);
    }

    /** Norm of the quaternion. Expressed by the formula
     *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
     * @return norm of <code>this</code> (norm is a real number)
     */
    public double norm() {
        return Math.sqrt(a * a + b * b + c * c + d * d);
    }

    /** Main method for testing purposes.
     * @param arg command line parameters
     */
    public static void main(String[] arg) {
//        Quaternion arv1 = new Quaternion(-1., 1, 2., -2.);
//        if (arg.length > 0)
//            arv1 = valueOf(arg[0]);
//        System.out.println("first: " + arv1.toString());
//        System.out.println("real: " + arv1.getRpart());
//        System.out.println("imagi: " + arv1.getIpart());
//        System.out.println("imagj: " + arv1.getJpart());
//        System.out.println("imagk: " + arv1.getKpart());
//        System.out.println("isZero: " + arv1.isZero());
//        System.out.println("conjugate: " + arv1.conjugate());
//        System.out.println("opposite: " + arv1.opposite());
//        System.out.println("hashCode: " + arv1.hashCode());
//        Quaternion res = null;
//        try {
//            res = (Quaternion) arv1.clone();
//        } catch (CloneNotSupportedException e) {
//        }
//        ;
//        System.out.println("clone equals to original: " + res.equals(arv1));
//        System.out.println("clone is not the same object: " + (res != arv1));
//        System.out.println("hashCode: " + res.hashCode());
//        res = valueOf(arv1.toString());
//        System.out.println("string conversion equals to original: "
//                + res.equals(arv1));
//        Quaternion arv2 = new Quaternion(1., -2., -1., 2.);
//        if (arg.length > 1)
//            arv2 = valueOf(arg[1]);
//        System.out.println("second: " + arv2.toString());
//        System.out.println("hashCode: " + arv2.hashCode());
//        System.out.println("equals: " + arv1.equals(arv2));
//        res = arv1.plus(arv2);
//        System.out.println("plus: " + res);
//        System.out.println("times: " + arv1.times(arv2));
//        System.out.println("minus: " + arv1.minus(arv2));
//        double mm = arv1.norm();
//        System.out.println("norm: " + mm);
//        System.out.println("inverse: " + arv1.inverse());
//        System.out.println("divideByRight: " + arv1.divideByRight(arv2));
//        System.out.println("divideByLeft: " + arv1.divideByLeft(arv2));
        System.out.println(valueOf("-1-2i-3j-4k"));
    }
}
// end of file
